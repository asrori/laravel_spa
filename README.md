# Laravel SPA (Single Page Application)

> Laravel SPA (Single Page Application) starter project template with Vuetify (VueJs).

## Setup Laravel

- `git clone https://gitlab.com/asrori/laravel_spa.git`
- `enter folder project ==> cd laravel_spa`
- `copy .env.example ==> .env`
- `composer install`
- `php artisan key:generate`
- Edit `.env` and set your database connection details
- `php artisan migrate`

### Running Server Laravel For Development
```bash
php artisan serve
serve running in http://localhost:8000/
```

## Setup Frontend (VueJs)
- `npm install` / `yarn`
- `open file laravel_spa/resources/js/config/api.js in editor`
- `in api.js edit variable HOST=http://localhost:8000/api/ (adjust url with your server laravel running)`

### Running Mix Frontend (Development)

```bash
# build and watch
npm run watch
```

### Running Mix Frontend (Production)

```bash
npm run production
```

// Drawer
export const DRAWER = 'DRAWER'

// Alert
export const SHOW_SNACKBAR = 'SHOW_SNACKBAR'
export const HIDE_SNACKBAR = 'HIDE_SNACKBAR'

import Vue from 'vue'
import Vuex from 'vuex'
import {
  DRAWER,
  SHOW_SNACKBAR,
  HIDE_SNACKBAR
} from './mutation-types'

Vue.use(Vuex)

const initialState = {
  is_dev: process.env.NODE_ENV === 'development',
  drawer: true,
  snackbar: {
    show: false,
    timeout: 6000,
    message: null,
    color: 'black'
  }
}

const state = initialState

const actions = {
  showSnackbar ({ commit }, snackbar) {
    commit(SHOW_SNACKBAR, snackbar)
    setTimeout(function () {
      commit(HIDE_SNACKBAR)
    }, 6000)
  },
  handleError ({ state, commit, dispatch, rootState, getters }, err) {
    if (err.response.status === 401) {
      dispatch('logout')
    }
  }
}

const mutations = {
  [DRAWER] (state, status) {
    state.drawer = status
  },
  [SHOW_SNACKBAR] (state, snackbar) {
    state.snackbar = {
      show: true,
      timeout: 6000,
      color: snackbar.color ? snackbar.color : 'black',
      msg: snackbar.msg
    }
  },
  [HIDE_SNACKBAR] (state) {
    state.snackbar.show = false
  }
}

const getters = {
  is_dev: state => {
    return state.is_dev
  },
  drawer: state => {
    return state.drawer
  },
  snackbar: state => {
    return state.snackbar
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters
})

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VeeValidate from 'vee-validate'
import './plugins/vuetify'

Vue.use(VeeValidate, { inject: false })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

const HOST = 'http://localhost:8000/api/'

// master endpoint
export const categories = `${HOST}categories`
export const items = `${HOST}items`

// options endpoint
const optionsPrefix = 'options/'
export const options = {
  categories: `${HOST}${optionsPrefix}categories`
}

import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import Vue from 'vue'
// import 'vuetify/src/stylus/app.styl'
import Vuetify from 'vuetify'
import { Scroll } from 'vuetify/lib/directives'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#ff9800'
  },
  directives: {
    Scroll
  }
})

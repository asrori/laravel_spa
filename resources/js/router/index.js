import Vue from 'vue'
import Router from 'vue-router'

// router
import Home from '~/views/Home.vue'
import About from '~/views/About.vue'
import categories from './categories'
import items from './items'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: { title: 'Beranda' }
    },
    {
      path: '/about-me',
      name: 'about',
      component: About,
      meta: { title: 'About' }
    },
    ...categories,
    ...items
  ]
})

import Index from '~/views/categories/Index.vue'
import Form from '~/views/categories/Form.vue'

export default [
  {
    path: '/categories',
    name: 'categories',
    component: Index,
    meta: { title: 'Categories' }
  },
  {
    path: '/categories/create',
    name: 'create-categories',
    component: Form,
    meta: { title: 'Create Categories' }
  },
  {
    path: '/categories/:id/edit',
    name: 'update-categories',
    component: Form,
    meta: { title: 'Update Categories' }
  }
]

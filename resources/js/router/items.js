import Index from '~/views/items/Index.vue'
import Form from '~/views/items/Form.vue'

export default [
  {
    path: '/items',
    name: 'items',
    component: Index,
    meta: { title: 'Items' }
  },
  {
    path: '/items/create',
    name: 'create-items',
    component: Form,
    meta: { title: 'Create Items' }
  },
  {
    path: '/items/:id/edit',
    name: 'update-items',
    component: Form,
    meta: { title: 'Update Items' }
  }
]

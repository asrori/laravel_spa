<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function ($route) {
	# route for items
    Route::get('/items', 'ItemsController@index');
    Route::get('/items/{id}', 'ItemsController@edit');
    Route::post('/items', 'ItemsController@create');
    Route::put('/items/{id}', 'ItemsController@update');
    Route::delete('/items/{id}', 'ItemsController@destroy');

    # route for categories
    Route::get('/categories', 'CategoriesController@index');
    Route::get('/categories/{id}', 'CategoriesController@edit');
    Route::post('/categories', 'CategoriesController@create');
    Route::put('/categories/{id}', 'CategoriesController@update');
    Route::delete('/categories/{id}', 'CategoriesController@destroy');

    # group for end-point options
    $route->group(['prefix' => 'options'], function ($app) {
        $app->get('/categories', 'CategoriesController@options');
    });
});

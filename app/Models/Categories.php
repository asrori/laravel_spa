<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    //
    use SoftDeletes;

    protected $table = "categories";
    protected $primaryKey = "id_category";

    protected $fillable = ["category_name", "is_active", "created_at", "updated_at", "deleted_at"];
    protected $dates = ["created_at", "updated_at", "deleted_at"];
}

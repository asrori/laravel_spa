<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Items extends Model
{
    //
    use SoftDeletes;

    protected $table = "items";
    protected $primaryKey = "id_item";

    protected $fillable = ["id_category", "item_name", "item_price", "sku_number", "min_stock", "is_active", "description", "created_at", "updated_at", "deleted_at"];
    protected $dates = ["created_at", "updated_at", "deleted_at"];

    protected $casts = [
        'item_price' => 'double',
        'min_stock'  => 'float'
    ];
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseApiController;
use Validator;

class CategoriesController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        # var request
        $key     = $request->get("key");
        $sortBy  = $request->get("sortby", "created_at");
        $orderBy = $request->get("orderby", "DESC");
        $perPage = (int) $request->get("per_page", 10);

        # get query
        $data = Categories::where("category_name", "like", "%".$key."%")->orderBy($sortBy, $orderBy)->paginate($perPage);

        # send result
        return $this->respond(200, $data, "Success");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        # set validator
        $validator = Validator::make($request->all(), [
            "category_name" => "required|max:255"
        ]);

        # validator failed
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # var request
        $data = $request->all();

        # query insert data
        $create = Categories::create([
            "category_name" => $data["category_name"],
            "is_active"     => $data["is_active"]
        ]);

        # if insert success
        if ($create) {
            return $this->respond(201, $create, "Categories Created Success");
        } else {
            return $this->respond(400, $create, "Categories Created Failed");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        # get query
        $categories = Categories::where("id_category", $id)->first();

        # check data if not null
        if ($categories) {
            return $this->respond(200, $categories, "Success");
        } else {
            return $this->respond(404, $categories, "Data Not Found");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        # set validator
        $validator = Validator::make($request->all(), [
            "category_name" => "required|max:255"
        ]);

        # validator failed
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # var request
        $data = $request->all();

        # get query
        $categories = Categories::where("id_category", $id)->first();

        # check data if not null
        if ($categories) {
            # query update data
            $update = $categories->update([
                "category_name" => $data["category_name"],
                "is_active"     => $data["is_active"]
            ]);

            # if update success
            if ($update) {
                return $this->respond(200, $update, "Categories Updated Success");
            } else {
                return $this->respond(400, $update, "Categories Updated Failed");
            }
        } else {
            return $this->respond(404, $categories, "Categories Not Found");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        # get query
        $categories = Categories::find($id);

        # check data if not null
        if ($categories) {
            $deleted = $categories->delete();
            return $this->respond(200, $deleted, "Success");
        } else {
            return $this->respond(404, $categories, "Data Not Found");
        }
    }

    public function options(Request $request)
    {
        $limit  = $request->get('limit');
        $search = $request->get('search');

        if ($limit == 0) {
            $data = Categories::get();
        } else {
            $data = Categories::limit($limit)->get();
        }

        if ($data->count() > 0)
        {
            foreach ($data as $val)
            {
                $list [] = [
                    'id'   => $val->id_category,
                    'text' => $val->category_name
                ];
            }
        }
        else
        {
            $list = [
                "id"   => '',
                "text" => 'Data Not Found'
            ];
        }

        return $list;
    }
}

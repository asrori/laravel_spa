<?php

namespace App\Http\Controllers\Api;

use App\Models\Items;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseApiController;
use Validator;

class ItemsController extends BaseApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        # var request
        $columnsSearch = ["item_name", "sku_number"];
        $key           = $request->get("key");
        $sortBy        = $request->get("sortby", "item_name");
        $orderBy       = $request->get("orderby", "DESC");
        $perPage       = (int) $request->get("per_page", 5);

        # get query
        $data = Items::select("id_item", "category_name", "item_name", "item_price", "sku_number", "min_stock", "items.is_active", "description")
        ->leftJoin("categories", "items.id_category", "=", "categories.id_category")
        ->where(function ($query) use ($columnsSearch, $key) {
            foreach ($columnsSearch as $column) {
                $query->orWhere($column, "like", "%".$key."%");
            }
        })
        ->orderBy($sortBy, $orderBy)
        ->paginate($perPage);

        # send result
        return $this->respond(200, $data, "Success");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        # set validator
        $validator = Validator::make($request->all(), [
            'id_category' => 'required',
            'item_name'   => 'required|max:100',
            'item_price'  => 'required|numeric',
            'sku_number'  => 'nullable|unique:items,sku_number,NULL,id_item,deleted_at,NULL',
            'min_stock'   => 'required|numeric',
        ]);

        # validator failed
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # var request
        $data = $request->all();

        # query insert data
        $create = Items::create([
            'id_category' => $data["id_category"],
            'item_name'   => $data["item_name"],
            'item_price'  => $data["item_price"],
            'sku_number'  => $data["sku_number"],
            'min_stock'   => $data["min_stock"],
            "is_active"   => $data["is_active"],
            "description" => $data["description"]
        ]);

        # if insert success
        if ($create) {
            return $this->respond(201, $create, "Items Created Success");
        } else {
            return $this->respond(400, $create, "Items Created Failed");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function show(Items $items)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        # get query
        $items = Items::where("id_item", $id)->first();

        # check data if not null
        if ($items) {
            return $this->respond(200, $items, "Success");
        } else {
            return $this->respond(404, $items, "Data Not Found");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        # set validator
        $validator = Validator::make($request->all(), [
            'id_category' => 'required',
            'item_name'   => 'required|max:100',
            'item_price'  => 'required|numeric',
            'sku_number'  => 'nullable|unique:items,sku_number,'.$id.',id_item,deleted_at,NULL',
            'min_stock'   => 'required|numeric',
        ]);

        # validator failed
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        # var request
        $data = $request->all();

        # get query
        $items = Items::where("id_item", $id)->first();

        # check data if not null
        if ($items) {
            # query update data
            $update = $items->update([
                'id_category' => $data["id_category"],
                'item_name'   => $data["item_name"],
                'item_price'  => $data["item_price"],
                'sku_number'  => $data["sku_number"],
                'min_stock'   => $data["min_stock"],
                "is_active"   => $data["is_active"],
                "description" => $data["description"]
            ]);

            # if update success
            if ($update) {
                return $this->respond(200, $update, "Items Updated Success");
            } else {
                return $this->respond(400, $update, "Items Updated Failed");
            }
        } else {
            return $this->respond(404, $items, "Items Not Found");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Items  $items
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        # get query
        $items = Items::find($id);

        # check data if not null
        if ($items) {
            $deleted = $items->delete();
            return $this->respond(200, $deleted, "Success");
        } else {
            return $this->respond(404, $items, "Data Not Found");
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseApiController extends Controller
{
    //
    protected function respond($status, $data, $msg) {
        $datas = ['msg' => $msg, 'data' => $data];

        return response()->json($datas, $status);
    }
}
